# syntax=docker/dockerfile:1

FROM openjdk:7-jre-alpine


COPY ./ .target

COPY run.sh ./



EXPOSE 8080


CMD ["./", "run.sh"]
